package ru.rencredit.jschool.kuzyushin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.TaskSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdListener extends AbstractListener {

    @NotNull
    private final TaskSoapEndpoint taskSoapEndpoint;

    @NotNull
    private final SessionService sessionService;

    @Autowired
    public TaskRemoveByIdListener(
            final @NotNull TaskSoapEndpoint taskSoapEndpoint,
            final @NotNull SessionService sessionService
    ) {
        this.taskSoapEndpoint = taskSoapEndpoint;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        sessionService.setListCookieRowRequest(taskSoapEndpoint);
        taskSoapEndpoint.removeTaskById(id);;
        System.out.println("[OK]");
    }
}

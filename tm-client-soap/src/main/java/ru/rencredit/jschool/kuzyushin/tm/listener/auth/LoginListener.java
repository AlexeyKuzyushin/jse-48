package ru.rencredit.jschool.kuzyushin.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.AuthSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class LoginListener extends AbstractListener {

    @NotNull
    private final AuthSoapEndpoint authSoapEndpoint;

    @NotNull
    private final SessionService sessionService;

    @Autowired
    public LoginListener(
            final @NotNull AuthSoapEndpoint authSoapEndpoint,
            final @NotNull SessionService sessionService
    ) {
        this.authSoapEndpoint = authSoapEndpoint;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login";
    }

    @Override
    @EventListener(condition = "@loginListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        authSoapEndpoint.login(login, password);
        sessionService.saveCookieHeaders(authSoapEndpoint);
        System.out.println("[OK]");
    }
}

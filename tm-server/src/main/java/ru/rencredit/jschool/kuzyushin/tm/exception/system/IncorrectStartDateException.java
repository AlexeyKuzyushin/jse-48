package ru.rencredit.jschool.kuzyushin.tm.exception.system;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

import java.util.Date;

public class IncorrectStartDateException extends AbstractException {

    public IncorrectStartDateException() {
        super("Error! Start Date is empty...");
    }

    public IncorrectStartDateException(final Date date) {
        super("Error! Start Date [" + date + "] is incorrect...");
    }
}

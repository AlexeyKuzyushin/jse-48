package ru.rencredit.jschool.kuzyushin.tm.exception.empty;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }
}

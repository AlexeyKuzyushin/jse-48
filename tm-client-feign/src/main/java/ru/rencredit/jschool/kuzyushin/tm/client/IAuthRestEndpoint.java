package ru.rencredit.jschool.kuzyushin.tm.client;

import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.rencredit.jschool.kuzyushin.tm.dto.Result;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;

import javax.jws.WebParam;

@FeignClient(name = "tm-server")
@RequestMapping(value="/rest/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public interface IAuthRestEndpoint {

    @GetMapping(value = "/login")
    public Result login(
            @WebParam(name = "username") final @Nullable String username,
            @WebParam(name = "password") final @Nullable String password
    );

    @GetMapping(value = "/profile")
    public UserDTO profile();

    @GetMapping(value = "/logout")
    public void logout();
}
